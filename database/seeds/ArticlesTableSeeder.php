<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0; $i < 5; $i++) 
        {
            $faker = Faker::create('App\Article');
            DB::table('articles')->insert([
                'title' => $faker->sentence,
                'content' => $faker->paragraph(5),
                'updated_at' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now()
    
    
            ]);

        }       

    }
    
}

